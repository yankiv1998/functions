const { list } = require("mocha/lib/reporters");

const getSum = (str1, str2) => {
  if (str1 === '') {
    str1 = 0;
  }
  if (str2 === '') {
    str2 = 0;
  }
  // add your implementation below
  if (Number.isNaN(parseInt(str1)) || Number.isNaN(parseInt(str2))) {
    return false;
  }

  return (Number(str1) + Number(str2)).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;
  listOfPosts.forEach(item => {
    if (item.author === authorName) {
      posts += 1
    }
    item.comments && item.comments.forEach(item => {
      if (item.author === authorName) {
        comments += 1;
      }
    })
  })
  // add your implementation below
  return `Post:${posts},comments:${comments}`;
};

const tickets = (people) => {
  let a25 = 0;
  let a50 = 0;
  for (let i = 0; i < people.length; i++) {
    if (people[i] === 25) {
      a25 += 1;
    }
    if (people[i] === 50) {
      a25 -= 1;
      a50 += 1;
    }

    if (people[i] === 100) {
      if (a50 === 0 && a25 >= 3) {
        a25 -= 3;
      } else {
        a25 -= 1;
        a50 -= 1;
      }
    }
    if (a25 < 0 || a50 < 0) {
      return 'NO'
    }
  }
  return 'YES';
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
